package com.example.demo.controller;

import com.example.demo.business.person.PersonBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.example.demo.controller.model.newPerson;
import com.example.demo.controller.model.Person;

import java.util.List;

@RestController
@Validated
public class PersonController {
    private final String version = "/api/v1";
    private final PersonBusiness personBusiness;
    @Autowired
    public PersonController(PersonBusiness personBusiness) {
        this.personBusiness = personBusiness;
    }
    @PostMapping(version+"/persons")
    public Person createPerson(@RequestBody newPerson person){
        return personBusiness.createPersonBusiness(person);
        //return new Person();
    }
    @DeleteMapping(version+"/persons/{id}")
    public Person deletePerson(@PathVariable int id){
        return personBusiness.deletePersonBusiness(id);

    }
    @PutMapping(version+"/persons/{id}")
    public Person updatePerson(@PathVariable int id, @RequestBody newPerson person){
        return personBusiness.updatePersonBusiness(id, person);
    }

    @GetMapping(version+"/persons/{id}")
    public List<Person> getOne(@PathVariable int id, @RequestParam(value = "nom", required = false) String nom){
        return personBusiness.getOne(id, nom);
    }

    @GetMapping(version+"/persons")
    public List<Person> getAll(@RequestParam(value = "nom", required = false) String nom){
        return personBusiness.getAll(nom);
    }
}
