package com.example.demo.dao.person;

import com.example.demo.controller.model.newPerson;
import com.example.demo.controller.model.Person;
import com.example.demo.dao.person.model.PersonDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonDAO {

    private static final String ID_FIELD = "id";
    private static final String NOM_FIELD = "nom";
    private static final String PRENOM_FIELD = "prenom";
    private static final String ANNEENAISSANCE_FIELD = "anneeNaissance";
    private static final String NATIONALITE_FIELD = "nationalite";
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonDAO(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        //Fais le lien entre la conf de yml et la BDD
    }

    private final RowMapper<PersonDTO> rowMapper = (rs, rowNum) -> {
        final PersonDTO person = new PersonDTO();
        person.setId(rs.getInt(ID_FIELD));
        person.setNom(rs.getString(NOM_FIELD));
        person.setPrenom(rs.getString(PRENOM_FIELD));
        person.setAnneeNaissance(rs.getInt(ANNEENAISSANCE_FIELD));
        person.setNationalite(rs.getString(NATIONALITE_FIELD));
        return person;};



    public Person create(newPerson person) {
        //insert in DB
        return new Person();
    }

    public Person delete(int id) {
        //delete from DB
        return new Person();
    }

    public Person update(int id, newPerson person) {

        return new Person();
    }

    public List<Person> readOne(int id, String nom) {

        List<Person> onePerson = null;
        List<PersonDTO> dtos = this.jdbcTemplate.query(
                "SELECT * FROM person WHERE id = " + id,
                this.rowMapper
        );

        if(dtos.size() == 1) {
            onePerson = new ArrayList<Person>();
            for(PersonDTO dto : dtos) {
                Person resp = new Person();
                resp.setId(dto.getId());
                resp.setNom(dto.getNom());
                resp.setPrenom(dto.getPrenom());
                resp.setAge(dto.getAnneeNaissance());
                resp.setNationalite(dto.getNationalite());
                onePerson.add(resp);
            }
        }
        return onePerson;
    }

    public List<Person> readAll(String nom) {

        List<Person> listPerson = null;
        List<PersonDTO> dtos = this.jdbcTemplate.query(
                "select*from person",
                this.rowMapper
        );

        if(dtos != null && dtos.size() > 0) {
            listPerson = new ArrayList<Person>();
            for(PersonDTO dto : dtos) {
                Person resp = new Person();
                resp.setId(dto.getId());
                resp.setNom(dto.getNom());
                resp.setPrenom(dto.getPrenom());
                resp.setAge(dto.getAnneeNaissance());
                resp.setNationalite(dto.getNationalite());
                listPerson.add(resp);
            }
        }
        return listPerson;
    }
}
