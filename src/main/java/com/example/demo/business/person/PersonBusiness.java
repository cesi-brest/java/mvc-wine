package com.example.demo.business.person;
import com.example.demo.controller.model.newPerson;
import com.example.demo.controller.model.Person;
import com.example.demo.dao.person.PersonDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonBusiness {
    private final PersonDAO personDAO;

    @Autowired
    public PersonBusiness(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }
    public Person createPersonBusiness(newPerson person) {
        return personDAO.create(person);
    }

    public Person deletePersonBusiness(int id) {
        return personDAO.delete(id);
    }

    public Person updatePersonBusiness(int id, newPerson person) {
        return personDAO.update(id, person);
    }

    public List<Person> getOne(int id, String nom) {
        return personDAO.readOne(id, nom);
    }

    public List<Person> getAll(String nom) {
        return personDAO.readAll(nom);
    }
}
